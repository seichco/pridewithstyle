module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    autoprefixer: {
      options: {
        browsers: ["> 0.01%"]
      },

      files: {
        'public/css/style.css': 'public/css/style.css'
      }
    }
  });

  grunt.loadNpmTasks('grunt-autoprefixer');
}