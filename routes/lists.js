var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    res.render('lists/index', {
        title: 'Your MailChimpLists',
        lists: data.data
    });
});

router.get('/:id', function(req, res) {
   res.render('index', res.email);
});

router.post('/lists/:id/subscribe', list.subscribe);

module.exports = router;
/*
 *  GET list of lists
 *  TODO: Remove
 */
exports.list = function(req, res) {
  mc.lists.list({}, function(data){
      res.render('lists/index', {
          title: 'Your MailChimpLists',
          lists: data.data
      });
  });
};

/*
 * GET info on a list
 * TODO: Remove
 */




/*
 * POST subscribe an email to a list
 */
exports.subscribe = function(req, res) {
  mc.lists.subscribe({
      id: req.params.id,
      email: {
          email: req.body.email
      }
  }, function(data) {
      req.session.success_flash = 'User subscribed successfully!';
      res.redirect('/lists/' + req.params.id);
  }, function(error) {
      if (error.error) {
          req.session.error_flash = error.code + ': ' + error.error;
      } else {
          req.session.error_flash = 'There was an error subscribing that email';
      }

      res.redirect('/lists/' + req.params.id);
  });
};