var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
  var req = req;
  res.render('index', { req: req });
});

// Dev list 74964c4bf5
// Prod list 4eeea892dc
router.post('/subscribe', function(req, res){
  mc.lists.subscribe({
    id: '4eeea892dc',
    email: {
      email: req.body.email
    },
    double_optin: false,
    send_welcome: false
  }, function(data) {
    req.session.alerts = {
      type: 'success flash-alert',
      message: req.body.email + ' added!'
    };
    res.redirect('/');
  }, function(error) {
    if (error.error) {
      req.session.alerts = {
        type: 'error flash-alert',
        code: error.code,
        message: error.code == 214 ? req.body.email + ' is already on the list.' : error.error + '.'
      };
    } else {
      req.session.alerts = {
        type: 'error flash-alert',
        message: 'There was an error adding your email.'
      };
    }
    res.redirect('/');
  });
});

module.exports = router;